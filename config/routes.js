/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  '/': { view: 'pages/homepage' },
  'POST /playback/change-radio': { action: 'playback/change-radio' },
  'POST /playback/play': { action: 'playback/play' },
  'POST /playback/pause': { action: 'playback/pause' },
  'POST /playback/volume': { action: 'playback/volume' },

  'GET /events': { action: 'events/events' },
  'GET /status': { action: 'status/status' },

  'GET /radio': { action: 'radio/all' },
  'GET /radio/:id': { action: 'radio/find' },
  'POST /radio': { action: 'radio/create' },
  'DELETE /radio/:id': { action: 'radio/destroy' },
  'PATCH /radio/:id': { action: 'radio/update' },
};
