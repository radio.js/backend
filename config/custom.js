/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {

  sseKeepalive: 15000,
  mpc: {
    hostname: 'localhost',
    port: 6600,
  }

};
