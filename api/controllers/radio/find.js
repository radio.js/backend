module.exports = {
  friendlyName: 'Find',
  description: 'Find radio.',
  inputs: {
    id: {
      description: 'The id of the radio to find',
      type: 'string',
      required: true,
    }
  },
  exits: {
    success: {
      description: 'The radio was found',
    },
    notFound: {
      description: 'A radio with the given ID could not be found',
      statusCode: 404,
    },
  },
  fn: async function ({id}, exits) {
    const radio = await Radio.findOne({id});
    if (radio) {
      return exits.success(radio);
    } else {
      return exits.notFound();
    }
  }
};
