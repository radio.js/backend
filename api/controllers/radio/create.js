module.exports = {
  friendlyName: 'Create',
  description: 'Create radio.',
  inputs: {
    name: {
      description: 'The name of the radio',
      type: 'string',
      required: true,
    },
    url: {
      description: 'The URL of the radio',
      type: 'string',
      required: true,
    },
    tags: {
      description: 'Tags for the radio',
      type: ['string'],
      required: false,
      allowNull: true,
    }
  },
  exits: {
    success: {
      description: 'The radio was successfully created',
    },
    invalidData: {
      description: 'The user passed invalid data',
      statusCode: 400,
    },
    error: {
      description: 'An unknown error occurred',
      statusCode: 500,
    }
  },
  fn: async function ({name, url, tags}, exits) {
    const cleanTags = sails.helpers.cleanTagsAttribute(tags);
    try {
      const createdRadio = await Radio.create({name, url, tags: cleanTags}).fetch();
      sails.emit(`hook:dataListener:create`, createdRadio);
      return exits.success(createdRadio);
    } catch (e) {
      if (e.name === 'UsageError') {
        return exits.invalidData();
      } else {
        return exits.error();
      }
    }
  }
};
