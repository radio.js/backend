module.exports = {
  friendlyName: 'Update',
  description: 'Update radio.',
  inputs: {
    id: {
      description: 'The id of the radio to update',
      type: 'string',
      required: true,
    },
    name: {
      description: 'The new name of the radio',
      type: 'string',
      required: false,
    },
    url: {
      description: 'The new URL of the radio',
      type: 'string',
      required: false,
    },
    tags: {
      description: 'Tags for the radio',
      type: ['string'],
      required: false,
      allowNull: true,
    }
  },
  exits: {
    success: {
      description: 'The radio was successfully updated',
    },
    notFound: {
      description: 'A radio with the given ID could not be found',
      statusCode: 404,
    },
    invalidData: {
      description: 'The user passed invalid data',
      statusCode: 400,
    },
    error: {
      description: 'An unknown error occurred',
      statusCode: 500,
    }
  },
  fn: async function ({id, name, url, tags}, exits) {
    const cleanTags = sails.helpers.cleanTagsAttribute(tags);
    try {
      const updatedRadio = await Radio.updateOne({id}).set({name, url, tags: cleanTags});
      if (updatedRadio) {
        sails.emit(`hook:dataListener:update`, updatedRadio);
        return exits.success(updatedRadio);
      } else {
        return exits.notFound();
      }
    } catch (e) {
      if (e.name === 'UsageError') {
        return exits.invalidData();
      } else {
        return exits.error();
      }
    }
  }
};
