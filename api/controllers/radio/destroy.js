module.exports = {
  friendlyName: 'Destroy radio',
  description: 'Destroy the radio with the given id',
  inputs: {
    id: {
      description: 'The id of the radio to delete',
      type: 'string',
      required: true,
    }
  },
  exits: {
    success: {
      description: 'The radio was successfully deleted',
    },
    notFound: {
      description: 'A radio with the given ID could not be found',
      statusCode: 404,
    }
  },
  fn: async function ({id}, exits) {
    const destroyedRadio = await Radio.destroyOne({id});
    if (destroyedRadio) {
      sails.emit(`hook:dataListener:destroy`, destroyedRadio);
      return exits.success(destroyedRadio);
    } else {
      return exits.notFound();
    }
  }
};
