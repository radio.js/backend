module.exports = {
  friendlyName: 'Pause',
  description: 'Pauses the radio playback',
  fn: async function () {
    await sails.hooks.mpc.pause();
  }
};
