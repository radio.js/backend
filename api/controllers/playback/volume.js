module.exports = {
  friendlyName: 'Volume',
  description: 'Sets the playback volume',
  inputs: {
    volume: {
      type: 'number'
    }
  },
  exits: {
    success: {
      description: 'The volume was changed successfully'
    },
    notInteger: {
      description: 'The given value isn\'t an integer',
      statusCode: 400
    },
    invalidRange: {
      description: 'The given volume isn\'t in the range 0-100',
      statusCode: 400
    }
  },
  fn: async function ({volume}, exits) {
    if (!Number.isInteger(volume)) {
      throw 'notInteger';
    }

    if (volume < 0 || volume > 100) {
      throw 'invalidRange';
    }

    await sails.hooks.mpc.changeVolume(volume);
    return exits.success();
  }
};
