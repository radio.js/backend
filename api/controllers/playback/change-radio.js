module.exports = {

  friendlyName: 'Change radio',

  description: 'Changes the currently playing radio',

  inputs: {
    id: {
      description: 'The id of the radio to play',
      type: 'string',
      required: true,
    }
  },

  exits: {
    success: {
      description: 'The radio was changed successfully'
    },
    notFound: {
      description: 'The requested radio id was not found',
      statusCode: 404
    },
    mpdException: {
      description: 'An exception on MPD has occurred',
    }
  },

  fn: async function ({id}, exits) {
    const radio = await Radio.findOne(id);

    if (typeof radio === 'undefined') {
      throw 'notFound';
    }

    try {
      await sails.hooks.mpc.changeRadio(radio);
      await sails.hooks.mpc.play();
    } catch (e) {
      throw { mpdException: e };
    }

    return exits.success();
  }
};
