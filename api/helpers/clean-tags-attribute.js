module.exports = {
  friendlyName: 'Clean tags attribute',
  description: 'Cleans the tags attribute from the radio, changing null and undefined to an empty array',
  inputs: {
    tags: {
      type: 'ref',
      description: 'The tags value to be cleaned',
      required: false,
    },
  },
  sync: true,
  fn: function (inputs, exits) {
    const isTagsAttributeValid = !(_.isNull(inputs.tags) || _.isUndefined(inputs.tags));
    const cleanTags = isTagsAttributeValid ? inputs.tags : [];
    return exits.success(cleanTags);
  },
};
