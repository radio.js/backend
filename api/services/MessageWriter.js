class MessageWriter {

  constructor(req, res) {
    this.res = res;
    this.onDisconnect = null;

    req.on('close', () => this._onDisconnectListener());
  }

  writeMessage(message, event = null) {
    if (event !== null) {
      this.res.write(`event: ${event}\n`);
    }

    const messageInNewLines = message.split('\n');
    messageInNewLines.forEach(string => {
      this.res.write(`data: ${string}\n`);
    });

    this.res.write('\n');
  }

  endStream() {
    this.res.end();
  }

  sendComment(message) {
    this.res.write(`: ${message}\n\n`);
  }

  sendPing() {
    this.sendComment('keepalive');
  }

  _onDisconnectListener() {
    if (typeof this.onDisconnect === 'function') {
      this.onDisconnect();
    }
  }
}

module.exports = MessageWriter;
